
tupla_vacia = tuple()

tupla_numeros = (10,20,30)
print(tupla_numeros)

n1 = tupla_numeros[0]
print(n1)
n2 = tupla_numeros[1]
print(n2)
#No hacer
#tupla_numeros[0] = 50

tupla_nombres = 'María', 'Ricardo'
print(tupla_nombres)
#Desempaquetando una tupla utilizando 'asignación múltiple'
nombre_1, nombre_2 = tupla_nombres
print(nombre_1)
print(nombre_2)

print('------------------------------------------')

dict_personas_1 = {
    'nombre': 'Juan'
}
dict_personas_2 = {
    'nombre': 'Liliana'
}

tupla_diccionarios = (dict_personas_1, dict_personas_2)
print(tupla_diccionarios)

dict_personas_1['apellido'] = 'Castro'
print(tupla_diccionarios)

dict_personas_1 = {'Test': 'Misiín Tic'}
print(tupla_diccionarios)


tupla_sencilla = 50,
print('Tupla sencilla-> ', tupla_sencilla)

def retorna_numeros():
    #procesos...
    return 100, 250

n1, n2 = retorna_numeros()

print("n1-> ", n1)